mkv Scripts

auto_mkvmerge.ps1

Description:  This PowerShell script will allow for batch processing of files with the mkvmerge.exe toolset.

Requirements:  mkvmerge.exe toolset (https://mkvtoolnix.download/)

Note:  The script assumes the mkvmerge toolset directory is in your PATH enviroment variable (this is not done on a default install).
